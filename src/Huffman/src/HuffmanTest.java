import java.io.IOException;
import java.util.LinkedList;

public class HuffmanTest {
    public static void main(String[] args) throws IOException {
        HuffmanTree h = new HuffmanTree();
        h.read("D:\\Huffman","test1.txt");
        LinkedList temp = h.createTree();
        h.getCode((HuffNode) temp.get(0),"");
        h.Encode("D:\\Huffman\\test2.txt");

        HuffmanTree r = new HuffmanTree();
        r.read2("D:\\Huffman","test2.txt");
        LinkedList temp2 = r.createTree2();
        r.getCode((HuffNode) temp2.get(0),"");
        r.Decode("D:\\Huffman\\test3.txt");
    }
}