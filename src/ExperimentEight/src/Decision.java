import java.util.Scanner;

public class Decision {
    private LinkedBinaryTree<String> tree;

    public Decision(){
        String e1 = "你喜欢玩游戏吗？";
        String e2 = "你喜欢用手机玩游戏吗？";
        String e3 = "你很自律，不过偶尔还是要点游戏来放松生活。";
        String e4 = "那你玩过王者荣耀吗？ ";
        String e5 = "看来你喜欢电脑游戏，手机游戏偶尔也能带来不一样的体验噢！";
        String e6 = "王者荣耀很有趣吧，希望你能在峡谷里愉快地玩耍！";
        String e7 = "王者荣耀是一款很不错的手游呢，推荐你了解一下。";

        LinkedBinaryTree<String> n2,n3,n4,n5,n6,n7;
        n3 = new LinkedBinaryTree<String>(e3);
        n5 = new LinkedBinaryTree<String>(e5);
        n6 = new LinkedBinaryTree<String>(e6);
        n7 = new LinkedBinaryTree<String>(e7);
        n4 = new LinkedBinaryTree<String>(e4,n6,n7);
        n2 = new LinkedBinaryTree<String>(e2,n4,n5);

        tree = new LinkedBinaryTree<String>(e1,n2,n3);
    }

    public void diagose() throws Exception {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;
        while(current.size()>1)
        {
            System.out.println(current.getRootElement());
            if(scan.nextLine().equalsIgnoreCase("Y"))
                current = current.getLeft();
            else
                current = current.getRight();
        }
        System.out.println(current.getRootElement());
    }
}