import junit.framework.TestCase;
import org.junit.Test;

public class LinkedBinaryTreeTest extends TestCase {
    LinkedBinaryTree a = new LinkedBinaryTree(1);
    LinkedBinaryTree b = new LinkedBinaryTree(2);
    LinkedBinaryTree c = new LinkedBinaryTree(3,a,b);
    LinkedBinaryTree d = new LinkedBinaryTree(4);
    LinkedBinaryTree e = new LinkedBinaryTree(5,c,d);
    LinkedBinaryTree f = new LinkedBinaryTree();
    LinkedBinaryTree x1 = new LinkedBinaryTree(20);
    LinkedBinaryTree x2 = new LinkedBinaryTree(23);
    LinkedBinaryTree x3 = new LinkedBinaryTree(19,x1,x2);
    LinkedBinaryTree x4 = new LinkedBinaryTree(17);
    LinkedBinaryTree x5 = new LinkedBinaryTree(17,x3,x4);

    @Test
    public void testSize() {
        assertEquals(3,c.size());
        assertEquals(5,e.size());
    }

    public void testInorder() {//中序
        assertEquals("[1, 3, 2, 5, 4]",e.inorder().toString());
        assertEquals("[1, 3, 2]",c.inorder().toString());
    }

    public void testPreorder() {//先序
        assertEquals("[5, 1, 3, 2, 4]",e.preorder().toString());
        assertEquals("[3, 1, 2]",c.preorder().toString());
    }

    public void testPostorder() {//后序
        assertEquals("[1, 3, 2, 4, 5]",e.postorder().toString());
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[20, 19, 23, 17, 17]",x5.postorder().toString());
    }

    public void testLevelorder() throws EmptyCollectionException {//层次
        assertEquals("[5, 3, 4, 1, 2]",e.levelorder().toString());
        assertEquals("[3, 1, 2]",c.levelorder().toString());
    }

    public void testContains() throws Exception {
        assertEquals(false,e.contains(17));
        assertEquals(false,a.contains(17));
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(true,f.isEmpty());
    }

}