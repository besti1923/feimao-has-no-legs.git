package test;

import cn.edu.besti.cs1923.D2317.Searching;

public class SearchingTest {
    public static void main(String[] args) {

        int a[]=new int[]{0,1,2,3,4,5,6,7,8,23,17};
        Searching b = new Searching();
      //  b.linear(a,23);
      //  b.linear(a,10000);
      //  b.linear(a,0);
      //  b.linear(a,17);
        b.binarySearch(a,23);
        b.binarySearch(a,17);
        b.binarySearch(a,0);
        b.binarySearch(a,10000);
    }
}
