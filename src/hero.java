public abstract class hero extends king2 implements Comparable{
    public hero(String name,int temperature){
        super(name,temperature);
    }

    @Override
    public int compareTo(Object o){
        king2 temp =(king2)o;
        if(temp.gettemperature()>this.temperature){
            return 1;
        }else {
            return 0;
        }
    }
}
