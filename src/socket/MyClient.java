package com.socket;

import java.io.OutputStream;

import java.net.Socket;

public class MyClient {

    public static void main(String args[]) throws Exception {

// 要连接的服务端IP地址和端口

        String host = "172.16.43.77";

        int port = 10086;

// 与服务端建立连接

        Socket socket = new Socket(host, port);

// 建立连接后获得输出流

        OutputStream outputStream = socket.getOutputStream();

        String message="欢迎来到地狱的入口！";

        socket.getOutputStream().write(message.getBytes("UTF-8"));

        outputStream.close();

        socket.close();

    }

}
