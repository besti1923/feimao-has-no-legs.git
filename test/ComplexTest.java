import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

public class ComplexTest extends TestCase {
    Complex complex = new Complex(1,1);
    @Test
    public void testAdd(){
        assertEquals(new Complex(3.0,3.0),complex.complexAdd(new Complex(2.0,2.0)));
    }
    @Test
    public void testSub(){
        assertEquals(new Complex(-5.0,-2.0),complex.ComplexSub(new Complex(6.0,3.0)));
    }
    @Test
    public void testMulti(){
        assertEquals(new Complex(3.0,2.0),complex.ComplexSub(new Complex(3.0,2.0)));
    }
    @Test
    public void testDiv(){
        assertEquals(new Complex(1.0,1.0),complex.ComplexSub(new Complex(1.0,1.0)));
        assertEquals(new Complex(0.0,0.0),complex.ComplexSub(new Complex(1.0,0.0)));
    }
}
