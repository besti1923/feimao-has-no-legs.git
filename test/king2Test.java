public class king2Test {
    public static void main(String[] args) {
        king2 machao = new king2("马超",0);
        king2 hanxin = new king2("韩信",1);
        machao.display();
        hanxin.display();

        if (machao.temperature < hanxin.temperature) {
            System.out.println(machao.getName() + "梯度更高");
        }
        else {
            System.out.println(hanxin.getName() + "梯度更高");
        }

        System.out.println("马超的哈希值为：" + machao.hashCode());
        System.out.println("韩信的哈希值为：" + hanxin.hashCode());
    }
}
